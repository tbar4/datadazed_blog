---
title: "{{ replace .Name "-" " " | title }}"
author: Trevor Barnes
date: {{ dateFormat "2006-01-02" .Date }}
description: ""
tags: ["", ""]
archives: ["{{ dateFormat "2006/01" .Date }}"]
draft: true
---